//
//  RootNavigationViewController.swift
//  SwiftSideMenu
//
//  Created by Evgeny Nazarov on 29.09.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

import UIKit
import CoreLocation

public class ENSideMenuNavigationController: UINavigationController, ENSideMenuProtocol,UINavigationControllerDelegate {
    
    public var sideMenu : ENSideMenu?
    public var sideMenuAnimationType : ENSideMenuAnimation = .Default
    
    
    // MARK: - Life cycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    public init( menuViewController: UIViewController, contentViewController: UIViewController?) {
        super.init(nibName: nil, bundle: nil)
        
        if (contentViewController != nil) {
            self.viewControllers = [contentViewController!]
        }
        
        sideMenu = ENSideMenu(sourceView: self.view, menuViewController: menuViewController, menuPosition:.Left)
        view.bringSubviewToFront(navigationBar)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        self.navigationItem.hidesBackButton = true;
        if(self.viewControllers.count == 1 || viewController is ProfileViewController || viewController is HomeTableViewController){
            let leftBarButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "SideMenu"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(MainNavigationController.onSideMenuPressed))
            viewController.navigationItem.leftBarButtonItem = leftBarButton;
        }else{
            let leftBarButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "BackButton"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(MainNavigationController.onBackButtonPressed))
            viewController.navigationItem.leftBarButtonItem = leftBarButton;
        }
    }
    
    func onBackButtonPressed (){
        self.popViewControllerAnimated(true)
    }
    
    func onSideMenuPressed (){
        self.sideMenu?.showSideMenu()
        
    }
    
    // MARK: - Navigation
    public func setContentViewController(contentViewController: UIViewController) {
        self.sideMenu?.toggleMenu()
        switch sideMenuAnimationType {
        case .None:
            self.viewControllers = [contentViewController]
            break
        default:
            gotoDestination(contentViewController)
            break
        }
    }
    
    func gotoDestination(destViewController:UIViewController){
        let currentViewController: UIViewController = AppDelegate.sharedInstance().mainNavigation.topViewController!
        if(String.fromCString(class_getName(destViewController.dynamicType)) == String.fromCString(class_getName(currentViewController.dynamicType))!){
            
        }else{
            let controllers : NSMutableArray = NSMutableArray(array: AppDelegate.sharedInstance().mainNavigation.viewControllers)
            controllers.addObject(destViewController)
            self.setViewControllers(controllers as! [UIViewController], animated: true)
        }
        
    }
    
    
}
