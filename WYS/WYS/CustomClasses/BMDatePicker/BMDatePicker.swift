//
//  BMDatePicker.swift
//  WYS
//
//  Created by Bala Murugan on 8/2/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class BMDatePicker: UIView {

    var textField: UITextField!
    var datePicker: UIDatePicker!
    var toolbar: UIToolbar!

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.datePicker = UIDatePicker(frame: CGRectMake(0, 0, screenSize.width, 180))
        datePicker.addTarget(self, action: #selector(datePickerDateChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        self.toolbar = UIToolbar(frame: CGRectMake(0, 0, screenSize.width, 44))
        
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(donePressed))
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil), barbuttonDone]
        
        self.addSubview(datePicker)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBAction func datePickerDateChanged(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = datePicker.datePickerMode == .Time ? "hh:mm a" : "MMM dd, yyyy"
        textField.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    func donePressed() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = datePicker.datePickerMode == .Time ? "hh:mm a" : "MMM dd, yyyy"
        textField.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
        textField.resignFirstResponder()
    }
    class func addDatePickerForTextField(textField: UITextField) {
        
        let dateListView = BMDatePicker(frame: CGRectMake(0, 0, screenSize.width, 180))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.minimumDate = nil
        dateListView.datePicker.maximumDate = NSDate()
        dateListView.datePicker.datePickerMode = UIDatePickerMode.Date
        let dateString = "1 Jan 1980"
        let df = NSDateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.dateFromString(dateString)
        if let unwrappedDate = date {
            dateListView.datePicker.setDate(unwrappedDate, animated: false)
        }
    }

}
