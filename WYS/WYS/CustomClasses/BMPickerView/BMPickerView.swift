//
//  BMPickerView.swift
//  WYS
//
//  Created by Bala Murugan on 8/2/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class BMPickerView: UIView {

    var textField: UITextField!
    var pickerView: UIPickerView!
    var toolbar: UIToolbar!
    
   // var arrayStates: [String]!
    var arrayData : [String]!

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.pickerView = UIPickerView(frame: CGRectMake(0, 0, screenSize.width, 180))
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.showsSelectionIndicator = true
        self.toolbar = UIToolbar(frame: CGRectMake(0, 0, screenSize.width, 44))
        
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(donePressed))
        barbuttonDone.tintColor = UIColor.blackColor()
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil), barbuttonDone]
        self.addSubview(pickerView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func donePressed() {
        textField.text = arrayData[pickerView.selectedRowInComponent(0)]
        textField.resignFirstResponder()
    }
    class func addPickerForTextField(textField: UITextField , data : [String]!) {
        let listView = BMPickerView(frame: CGRectMake(0, 0, screenSize.width, 180))
        textField.inputView = listView
        textField.inputAccessoryView = listView.toolbar
       // textField.text = data[0]
        listView.arrayData = data
        listView.pickerView.reloadAllComponents()
        listView.textField = textField
    }
}
extension BMPickerView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayData.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayData[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textField.text = arrayData[row]
    }
}