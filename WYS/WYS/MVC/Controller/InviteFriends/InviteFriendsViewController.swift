//
//  InviteFriendsViewController.swift
//  WYS
//
//  Created by Bala Murugan on 10/23/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit
import SMTPLite

class InviteFriendsViewController: UIViewController {

    @IBOutlet weak var textfieldEmail : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "INVITE FRIENDS"
        textfieldEmail.setLeftPadding(20)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    @IBAction func onSendEmailButtonPressed (withSender sender : UIButton){
        if textfieldEmail.isEmpty || !textfieldEmail.text!.isValidEmail{
            let alert = BMExtension.alert("Please enter the valid email")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            sendEmail()
        }

    }
    
    
    func sendEmail()  {
        SVProgressHUD.showWithStatus("Sending email...")
        let message : SMTPMessage = SMTPMessage()
        message.from = "wysconnect@gmail.com"
        message.to = textfieldEmail.text!
        message.host = "smtp.gmail.com"
        message.account = "wysconnect@gmail.com"
        message.pwd = "Patricksmith"
        
        let htmlString : String = "<p>Please click the below link to download WYS application</p><p><a title=View on Appstore href=https://itunes.apple.com/us/app/wys-app/id1168512916?ls=1&amp;mt=8>https://itunes.apple.com/us/app/wys-app/id1168512916?ls=1&amp;mt=8</a></p>"
        message.subject = "Join WYS Application"
        message.content = htmlString
        message.send({ (messg, now, total) in
            
            }, success: { (messg) in
                dispatch_async(dispatch_get_main_queue(), {
                    self.textfieldEmail.text = ""
                    SVProgressHUD.showSuccessWithStatus("Email Sent")
                })
            }, failure: { (messg, error) in
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.showErrorWithStatus("Something went wrong. Please try again")
                })
        })

    }
}

extension InviteFriendsViewController : UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
