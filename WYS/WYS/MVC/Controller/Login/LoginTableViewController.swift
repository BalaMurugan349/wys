//
//  LoginTableViewController.swift
//  WYS
//
//  Created by Bala Murugan on 7/25/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class LoginTableViewController: UITableViewController {
    
    @IBOutlet weak var textfieldUsername : UITextField!
    @IBOutlet weak var textfieldPassword : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldUsername.setLeftPadding(20)
        textfieldPassword.setLeftPadding(20)
        let imgvw = UIImageView(image: UIImage(named: "BG"))
        imgvw.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        self.tableView.backgroundView = imgvw
        self.navigationController?.navigationBarHidden = true


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- BUTTON ACTIONS
    //MARK:- LOGIN BUTTON ACTION
    @IBAction func onLoginButtonAction (sender : UIButton){
        if textfieldUsername.isEmpty {
            let alert = BMExtension.alert("Please enter the username")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if textfieldPassword.isEmpty{
            let alert = BMExtension.alert("Please enter your password")
            self.presentViewController(alert, animated: true, completion: nil)

        }
        else{
            SVProgressHUD.showWithStatus("Processing...")
            var tokenstring = "12345"
            if let token = NSUserDefaults.standardUserDefaults().valueForKey("DeviceToken") as? String{
                tokenstring = token
            }

            Person.login(["username" : textfieldUsername.text! , "secret_key" : textfieldPassword.text!, "device_token" : tokenstring], success: { (result) in
                SVProgressHUD.dismiss()
                //print(result)
                AppDelegate.sharedInstance().didLogin()
                }, failure: { (error) in
                    SVProgressHUD.showErrorWithStatus(error!.localizedDescription)
            })
        }
        
    }
    
    //MARK:- FACEBOOK LOGIN BUTTON ACTION
    @IBAction func onFacebookLoginButtonAction (sender : UIButton){
        let login : FBSDKLoginManager = FBSDKLoginManager()
        login.loginBehavior = FBSDKLoginBehavior.Browser
        login.logOut()
        login.logInWithReadPermissions(["public_profile", "email","user_birthday"], fromViewController: self) { (result, error) -> Void in
            if(error == nil && result.isCancelled == false){
                SVProgressHUD.showWithStatus("Processing...")
                let request = FBSDKGraphRequest(graphPath:"me", parameters:["fields" : "id, name, email,picture.type(large),gender,birthday"])
                request.startWithCompletionHandler {
                    (connection, result, error) in
                    if error != nil {
                        SVProgressHUD.showErrorWithStatus(error.localizedDescription)
                    }
                    else if let userData = result as? [String:AnyObject] {
                       // print("\(userData)")
                        self.facebookLoginWithJson(userData)
                        
                    }
                }
            }else{
               // SVProgressHUD.showErrorWithStatus("Login Failed")
            }
        }

    }
    
    func facebookLoginWithJson(details : NSDictionary) {
        SVProgressHUD.showWithStatus("Processing...")
        var tokenstring = "12345"
        if let token = NSUserDefaults.standardUserDefaults().valueForKey("DeviceToken") as? String{
            tokenstring = token
        }

        let dict = ["name" : details["name"] as! String, "email" : details["email"] as! String, "fb_id" : details["id"] as! String, "dob" : "", "gender" : details["gender"] as! String, "device_token" : tokenstring ]
        let imageUrl : String = ((details["picture"] as! NSDictionary).objectForKey("data") as! NSDictionary).objectForKey("url") as! String
        let profileImage : UIImage = UIImage(data: NSData(contentsOfURL: NSURL(string: imageUrl)!)!)!
        Person.facebookLogin(dict, profilePic: profileImage, success: { (result) in
            SVProgressHUD.dismiss()
            AppDelegate.sharedInstance().didLogin()
            }) { (error) in
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
        }
        
    }
    //MARK:- REGISTER BUTTON ACTION
    @IBAction func onRegisterButtonAction (sender : UIButton){
        let reg : RegisterTableViewController = storyboard?.instantiateViewControllerWithIdentifier("RegisterVC") as! RegisterTableViewController
        self.navigationController?.pushViewController(reg, animated: true)
        
    }

}

extension LoginTableViewController : UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
