//
//  SendRequestTableViewController.swift
//  WYS
//
//  Created by Bala Murugan on 8/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SendRequestTableViewController: UITableViewController {
    
    var arrayUsers  : [Person] = [Person]()
    var _activityIndicatorView : UIActivityIndicatorView?
    var indexCount : NSInteger!
    var isLoadMoreData : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "WYS"
        indexCount = 0
        fetchUsers(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchUsers(showHUD : Bool)  {
        if indexCount == 0 && showHUD == true { SVProgressHUD.showWithStatus("Processing...") }
        Person.listUsers(["userid" : Person.currentPerson().userid , "index" : "\(indexCount)"], success: { (result) in
            self._activityIndicatorView?.stopAnimating()
            if (self.indexCount == 0){ self.arrayUsers.removeAll() }
            self.isLoadMoreData = self.arrayUsers.count % 15 == 0
            self.arrayUsers.appendContentsOf(result as! [Person])
            self.tableView.reloadData()
            SVProgressHUD.dismiss()
            }) { (error) in
                self._activityIndicatorView?.stopAnimating()
                self.isLoadMoreData = false
               if self.indexCount == 0 { SVProgressHUD.showErrorWithStatus(error?.localizedDescription) }
        }
    }
    
    override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if (self.tableView.contentOffset.y<0)/* scroll from top */
        {
            
        }else if (self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height) && arrayUsers.count > 0 && isLoadMoreData){
            indexCount = indexCount + 1
            self.setLoadingFooter()
            self.fetchUsers(false)
        }
    }

    //MARK:- SETTING FOOTER LOADING VIEW
    func setLoadingFooter (){
        let frame : CGRect =  CGRectMake(0, 0, self.view.frame.size.width, 30)
        let aframe : CGRect = CGRectMake(self.view.center.x - 10, 20, 5, 5)
        let loadingView : UIView = UIView(frame: frame)
        _activityIndicatorView = UIActivityIndicatorView(frame: aframe)
        _activityIndicatorView?.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        _activityIndicatorView?.startAnimating()
        loadingView.addSubview(_activityIndicatorView!)
        self.tableView.tableFooterView = loadingView
    }

    //MARK:- TABLE VIEW DELEGATES
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayUsers.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        if(cell == nil){
            cell = RelativesTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        let user = arrayUsers[indexPath.row]
        (cell as! RelativesTableViewCell).labelName.text = user.name
        (cell as! RelativesTableViewCell).imageViewProfile.image(user.profileImage)
        (cell as! RelativesTableViewCell).labelRelation.hidden = true
        (cell as! RelativesTableViewCell).labelPoints.hidden = true
        (cell as! RelativesTableViewCell).buttonAccept.setTitle("ADD", forState: UIControlState.Normal)
        (cell as! RelativesTableViewCell).buttonAccept.tag = indexPath.row
        (cell as! RelativesTableViewCell).buttonAccept.addTarget(self, action: #selector(RequestsTableViewController.onAcceptButonPressed(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        return cell!
    }
    
    
    //MARK:- ACCEPT BUTTON ACTION
    func onAcceptButonPressed (sender : UIButton){
        let user = arrayUsers[sender.tag]
        let alertController = UIAlertController(title: "WYS", message: "Please enter your relationship with \(user.name)", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addTextFieldWithConfigurationHandler { (txtfld) in
            BMPickerView.addPickerForTextField(txtfld, data: ["Peer","Supervisor","Subordinate","Friend","Family"])
        }
        let alertNoAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) { (action) -> Void in
        }
        let alertYesAction = UIAlertAction(title: "Done", style: UIAlertActionStyle.Default) { (action) -> Void in
            let txtfld : UITextField = alertController.textFields!.first!
            if txtfld.text != "" {
                let details : [String : String] = ["from_id" : Person.currentPerson().userid, "to_id" : user.userid, "relationship" : txtfld.text!]
                self.sendRequestwithJson(details)
            }
            
        }
        alertController.addAction(alertNoAction)
        alertController.addAction(alertYesAction)
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    func sendRequestwithJson(dict : [String : String])  {
        SVProgressHUD.showWithStatus("Processing...")
        Relationship.sendRequest(dict, success: { (result) in
            SVProgressHUD.showSuccessWithStatus("Request sent")
            self.indexCount = 0
            self.fetchUsers(false)
            }) { (error) in
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
        }
        
    }
    
}
