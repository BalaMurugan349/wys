//
//  RegisterTableViewController.swift
//  WYS
//
//  Created by Bala Murugan on 7/25/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RegisterTableViewController: UITableViewController {
    
    @IBOutlet var arrayTextfields : [UITextField]!
    @IBOutlet var imageViewProfile : UIImageView!
    var activeTextfield : UITextField!
    var imagePicker : UIImagePickerController!
    @IBOutlet var buttonProfileImage : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for textfld in arrayTextfields{
            textfld.setLeftPadding(20)
        }
        imageViewProfile.layer.cornerRadius = 50.0
        imageViewProfile.clipsToBounds = true
        BMDatePicker.addDatePickerForTextField(arrayTextfields[4])
        BMPickerView.addPickerForTextField(arrayTextfields[5], data: ["Male","Female","Other"])
        let imgvw = UIImageView(image: UIImage(named: "BG"))
        imgvw.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        self.tableView.backgroundView = imgvw
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK:- BUTTON ACTIONS
    //MARK:- BACK BUTTON ACTION
    @IBAction func onBackButtonPressed (){
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    @IBAction func onTermsButtonPressed (withSender sender : UIButton){
        let web : WebViewController = self.storyboard?.instantiateViewControllerWithIdentifier("WebViewVC") as! WebViewController
        web.fileName = "Terms"
        web.navTitle = "Terms and Conditions"
        let nav = UINavigationController(rootViewController: web)
        self.presentViewController(nav, animated: true, completion: nil)
    }
    
    @IBAction func onPrivacyButtonPressed (withSender sender : UIButton){
        let web : WebViewController = self.storyboard?.instantiateViewControllerWithIdentifier("WebViewVC") as! WebViewController
        web.fileName = "Privacy"
        web.navTitle = "Privacy Policy"
        let nav = UINavigationController(rootViewController: web)
        self.presentViewController(nav, animated: true, completion: nil)

    }
    
    //MARK:- REGISTER BUTTON ACTION
    @IBAction func onRegisterButtonPressed (sender : UIButton){
        if !verifyTextfields(){
            let alert = BMExtension.alert("Please enter all the required fields")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !arrayTextfields[2].isEmpty && !arrayTextfields[2].text!.isValidEmail{
            let alert = BMExtension.alert("Please enter the valid email")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if imageViewProfile.image == UIImage(named: "ProfilePlaceholder")
        {
            let alert = BMExtension.alert("Please select your profile picture")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            SVProgressHUD.showWithStatus("Processing...")
            var tokenstring = "12345"
            if let token = NSUserDefaults.standardUserDefaults().valueForKey("DeviceToken") as? String{
                tokenstring = token
            }
            let dict = ["name" : arrayTextfields[0].text!, "username" : arrayTextfields[1].text!,  "email" : arrayTextfields[2].isEmpty ? "" : arrayTextfields[2].text!, "secret_key" : arrayTextfields[3].text!, "dob" : arrayTextfields[4].isEmpty ? "" : arrayTextfields[4].text!, "gender" : arrayTextfields[5].isEmpty ? "" : arrayTextfields[5].text!,"device_token" : tokenstring]
            Person.signUp(dict, profilePic: imageViewProfile.image!, success: { (result) in
                SVProgressHUD.dismiss()
                AppDelegate.sharedInstance().didLogin()

                }, failure: { (error) in
                    SVProgressHUD.showErrorWithStatus(error!.localizedDescription)
                    
            })
        }
        
    }
    
    func verifyTextfields() -> Bool {
        var success : Bool = true
        for textfld in arrayTextfields {
            if textfld != arrayTextfields[2] && textfld != arrayTextfields[4] && textfld != arrayTextfields[5]{
            if textfld.isEmpty{
                success = false
                
            }
            }
        }
        return success
    }
    
    //MARK:- PROFILE IMAGE ACTION
    @IBAction func onProfileImagePressed (sender : UIButton){
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        if(activeTextfield != nil) { activeTextfield?.resignFirstResponder() }
        let optionMenu = UIAlertController(title: nil, message: "Choose", preferredStyle: .ActionSheet)
        
        let camera = UIAlertAction(title: "Camera", style: .Default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                let hasCamera : Bool = UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
                self.imagePicker.sourceType = hasCamera ? UIImagePickerControllerSourceType.Camera : UIImagePickerControllerSourceType.PhotoLibrary
                self.presentViewController(self.imagePicker, animated: true, completion: nil)
        })
        
        let album = UIAlertAction(title: "Photo Album", style: .Default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                self.presentViewController(self.imagePicker, animated: true, completion: nil)
                
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler:
            {
                (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(camera)
        optionMenu.addAction(album)
        optionMenu.addAction(cancelAction)
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
    }
    
}

//MARK:- TEXTFIELD DELEGATE
extension RegisterTableViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(textField: UITextField) {
        activeTextfield = textField
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

//MARK:- IMAGEPICKERCONTROLLER DELEGATE
extension RegisterTableViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        self.dismissViewControllerAnimated(true, completion: nil)
        var compression : CGFloat = 0.7
        let maxCompression : CGFloat = 0.1
        let maxFileSize : Int = 250*1024
        var imageData : NSData? = UIImageJPEGRepresentation(image, compression)
        while (imageData?.length > maxFileSize && compression > maxCompression)
        {
            compression -= 0.1
            imageData  = UIImageJPEGRepresentation(image, compression)
        }
        imageViewProfile.image = UIImage(data: imageData!)
        buttonProfileImage.setTitle("Change", forState: UIControlState.Normal)
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
}
