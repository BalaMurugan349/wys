//
//  SideMenuTableViewController.swift
//  WYS
//
//  Created by Bala Murugan on 7/25/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SideMenuTableViewController: UITableViewController {
    
    @IBOutlet var imageViewProfile : UIImageView!
    @IBOutlet var labelName : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewProfile.layer.cornerRadius = 50.0
        imageViewProfile.clipsToBounds = true
        imageViewProfile.image(Person.currentPerson().profileImage)
        labelName.text = Person.currentPerson().name
        
        let imgvw = UIImageView(image: UIImage(named: "BG"))
        imgvw.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        self.tableView.backgroundView = imgvw
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row{
        case 0 :
            let home : HomeTableViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HomeVC") as! HomeTableViewController
            self.sideMenuController()?.setContentViewController(home)
            break
            
        case 1 :
            let profile : ProfileViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ProfileVC") as! ProfileViewController
            self.sideMenuController()?.setContentViewController(profile)
            break
        case 2:
            let profile : SendRequestTableViewController = self.storyboard!.instantiateViewControllerWithIdentifier("SendRequestVC") as! SendRequestTableViewController
            self.sideMenuController()?.setContentViewController(profile)
            break
            
        case 3 :
            let invite : InviteFriendsViewController = self.storyboard!.instantiateViewControllerWithIdentifier("InviteFriendsVC") as! InviteFriendsViewController
            self.sideMenuController()?.setContentViewController(invite)

            break
            
        case 4 :
            showLogoutAlert()
            break
            
        default :
            self.sideMenuController()?.sideMenu?.toggleMenu()
            break
        }
    }
    
    func showLogoutAlert()  {
        let alertController = UIAlertController(title: "WYS", message: "Are you sure you want to Log out?", preferredStyle: UIAlertControllerStyle.Alert)
        let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default) { (action) -> Void in
            
        }
        let alertYesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.logoutWithJson()
        }
        
        alertController.addAction(alertNoAction)
        alertController.addAction(alertYesAction)
        AppDelegate.sharedInstance().mainNavigation.topViewController!.presentViewController(alertController, animated: true, completion: nil)
        self.sideMenuController()?.sideMenu?.toggleMenu()
        
        
        
    }
    
    func logoutWithJson()  {
        SVProgressHUD.showWithStatus("Processing...")
        Person.logout({ (result) in
            SVProgressHUD.dismiss()
            NSUserDefaults.standardUserDefaults().removeObjectForKey("LoggedUserDetails")
            NSUserDefaults.standardUserDefaults().synchronize()
            Person.logout()
            AppDelegate.sharedInstance().didLogout()

            }) { (error) in
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                
        }
    }
    
}
