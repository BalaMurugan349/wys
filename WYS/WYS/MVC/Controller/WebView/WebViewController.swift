//
//  WebViewController.swift
//  WYS
//
//  Created by Bala Murugan on 12/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class WebViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var webViewObj : UIWebView!
    var fileName : String!
    var navTitle : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //getting local file
        let localHtmlFile = NSBundle.mainBundle().URLForResource(fileName, withExtension: "docx");
        //creating request
        let request = NSURLRequest(URL: localHtmlFile!);
        //loading request
        webViewObj.loadRequest(request);
        
        self.navigationItem.title = navTitle
        let btnDone : UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(WebViewController.onDoneButtonPressed))
        self.navigationItem.rightBarButtonItem = btnDone

    }
    
    func onDoneButtonPressed()  {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        let alert = BMExtension.alert("\(error.localizedDescription)")
        self.presentViewController(alert, animated: true, completion: nil)

    }


}
