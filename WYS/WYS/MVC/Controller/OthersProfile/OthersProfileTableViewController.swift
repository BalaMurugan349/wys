//
//  OthersProfileTableViewController.swift
//  WYS
//
//  Created by Bala Murugan on 8/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class OthersProfileTableViewController: UITableViewController {
    
    var userDetails : Relationship!
    
    @IBOutlet weak var imageviewProfile : UIImageView!
    @IBOutlet var labelPoints : [UILabel]!
    @IBOutlet weak var buttonViewMore : UIButton!
    var arrayReference : NSMutableArray = NSMutableArray()
    var arrayRecentActivity : NSMutableArray = NSMutableArray()
    @IBOutlet weak var labelReference : UILabel!
    @IBOutlet weak var labelRecentActivity : UILabel!
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var buttonSetGoal : UIButton!
    @IBOutlet weak var labelEmail : UILabel!
    @IBOutlet weak var labelGender : UILabel!
    @IBOutlet weak var constraintRecentActivityHeight : NSLayoutConstraint!
    @IBOutlet weak var viewTableHeader : UIView!
    
    @IBOutlet weak var labelActivity1 : UILabel!
    @IBOutlet weak var labelActivity2 : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageviewProfile.layer.cornerRadius = 50.0
        imageviewProfile.clipsToBounds = true
        self.navigationItem.title = "PROFILE"
        getProfileDetails()
        

    }
    
    @IBAction func onMoreButtonPressed (withSender sender : UIButton){
        let recent : RecentActivityTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RecentActivityVC") as! RecentActivityTableViewController
        recent.arrayRecentActivity = arrayRecentActivity
        self.navigationController?.pushViewController(recent, animated: true)
    }
    
    func getProfileDetails()  {
        SVProgressHUD.showWithStatus("Processing...")
        let dict : [String : String] = ["from_id" : Person.currentPerson().userid , "to_id" : userDetails.userid]
        Profile.viewProfile(dict, success: { (result) in
            self.arrayReference.addObjectsFromArray(result["preferenceList"] as! NSArray as [AnyObject])
            self.arrayRecentActivity.addObjectsFromArray(result["RecentActivities"] as! NSArray as [AnyObject])
            self.tableView.reloadData()
            if self.arrayReference.count == 0 { self.labelReference.text = "No Reference List"}
            self.labelPoints[0].text = result["PointsGiven"] as? String
            self.labelPoints[1].text = result["PointsReceived"] as? String
            self.labelPoints[2].text = result["wysScore"] as? String
            self.loadUserData(result["profileDetails"] as! NSArray as [AnyObject])
            self.setRecentActivity(result["RecentActivities"] as! NSArray as [AnyObject])
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
        }
        
    }

    func loadUserData (userData : NSArray){
        let dictData : NSDictionary = userData[0] as! NSDictionary
        imageviewProfile.image(dictData["image"] as! String)
        labelName.text = dictData["name"] as? String
        labelEmail.text = dictData["email"] as? String
        labelGender.text = dictData["gender"] as? String
    }

    func  setRecentActivity(activity : NSArray)  {
        if activity.count == 0 {
        constraintRecentActivityHeight.constant = 0
        labelRecentActivity.text = "No Recent Activity"
        viewTableHeader.frame = CGRectMake(0, 0, screenSize.width, 490)
        self.tableView.tableHeaderView = viewTableHeader
        }else if activity.count == 1{
        let dict : NSDictionary = activity[0] as! NSDictionary
        labelActivity1.text = "Gave \(dict["points"] as! String)  pt"
        labelDate1.text = CheckForNull(strToCheck:NSDate.changeServerTimeZoneToLocalForDate(dict["date"] as! String))

        constraintRecentActivityHeight.constant = 45
        viewTableHeader.frame = CGRectMake(0, 0, screenSize.width, 535)
        self.tableView.tableHeaderView = viewTableHeader
        }else if activity.count > 1{
            let dict : NSDictionary = activity[0] as! NSDictionary
            labelActivity1.text = "Gave \(dict["points"] as! String)  pt"
            labelDate1.text = CheckForNull(strToCheck:NSDate.changeServerTimeZoneToLocalForDate(dict["date"] as! String))

            let dict1 : NSDictionary = activity[1] as! NSDictionary
            labelActivity2.text = "Gave \(dict1["points"] as! String)  pt"
            labelDate2.text = CheckForNull(strToCheck:NSDate.changeServerTimeZoneToLocalForDate(dict1["date"] as! String))

            
            if activity.count > 2 {
                constraintRecentActivityHeight.constant = 140
                viewTableHeader.frame = CGRectMake(0, 0, screenSize.width, 630)
                self.tableView.tableHeaderView = viewTableHeader
            }else{
                constraintRecentActivityHeight.constant = 90
                viewTableHeader.frame = CGRectMake(0, 0, screenSize.width, 580)
                self.tableView.tableHeaderView = viewTableHeader

            }
        }
        self.tableView.reloadData()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSetGoalButtonPressed (Sender : UIButton){
        let goalVC : GoalPointTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GoalVC") as! GoalPointTableViewController
        self.navigationController?.pushViewController(goalVC, animated: true)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayReference.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let dict : NSDictionary = arrayReference[indexPath.row] as! NSDictionary
        let name : String = dict["preference_name"] as! String

        let height = name.heightWithConstrainedWidth(screenSize.width, font: UIFont.systemFontOfSize(14)) + 20

        return height
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cel")
        if(cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cel")
        }
        let dict : NSDictionary = arrayReference[indexPath.row] as! NSDictionary
        cell?.textLabel?.text = dict["preference_name"] as? String
        cell?.textLabel?.textAlignment = NSTextAlignment.Center
        cell?.textLabel?.textColor = UIColor.whiteColor()
        cell?.textLabel?.font = UIFont.systemFontOfSize(14)
        cell?.textLabel?.numberOfLines = 0
        cell?.textLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        
        return cell!
        
    }
    
}
