//
//  RequestsTableViewController.swift
//  WYS
//
//  Created by Bala Murugan on 7/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RequestsTableViewController: UITableViewController {
    var arrayData : [Relationship] = [Relationship]()
    var isSearching : Bool = false
    var arraySearch : [Relationship] = [Relationship]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.fetchRequest(false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func startSearch(text : String) {
        let predicate : NSPredicate = NSPredicate(format: "name contains[C] %@", text)
        arraySearch = (arrayData as NSArray).filteredArrayUsingPredicate(predicate) as! [Relationship]
        self.tableView.reloadData()
        
    }
    
    func fetchRequest(showHUD : Bool)  {
        if showHUD == true { SVProgressHUD.showWithStatus("Processing...") }
        Relationship.viewRequest(["user_id" : Person.currentPerson().userid], success: { (result) in
            self.arrayData.removeAll()
            self.arrayData.appendContentsOf(result as! [Relationship])
            self.tableView.reloadData()
            SVProgressHUD.dismiss()
            }) { (error) in
                self.arrayData.removeAll()
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
               // SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
        }
        
        
        
    }

    //MARK:- TABLEVIEW DELEGATES
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  isSearching ? arraySearch.count : arrayData.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        if(cell == nil){
            cell = RelativesTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        let data : Relationship = isSearching ? arraySearch[indexPath.row]  : arrayData[indexPath.row]
        (cell as! RelativesTableViewCell).labelName.text = data.name
        (cell as! RelativesTableViewCell).labelRelation.hidden = true
        (cell as! RelativesTableViewCell).labelPoints.hidden = true
        (cell as! RelativesTableViewCell).buttonAccept.tag = indexPath.row
        (cell as! RelativesTableViewCell).imageViewProfile.image(data.profilePic)
        (cell as! RelativesTableViewCell).buttonAccept.addTarget(self, action: #selector(RequestsTableViewController.onAcceptButonPressed(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        return cell!
    }
    
    
    //MARK:- ACCEPT BUTTON ACTION
    func onAcceptButonPressed (sender : UIButton){
        let data : Relationship = isSearching ? arraySearch[sender.tag]  : arrayData[sender.tag]
        let alertController = UIAlertController(title: "WYS", message: "Please enter your relationship with \(data.name)", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addTextFieldWithConfigurationHandler { (txtfld) in
           // txtfld.keyboardType = UIKeyboardType.Default
            BMPickerView.addPickerForTextField(txtfld, data: ["Peer","Supervisor","Subordinate","Friend","Family"])
        }
        let alertNoAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) { (action) -> Void in
        }
        let alertYesAction = UIAlertAction(title: "Done", style: UIAlertActionStyle.Default) { (action) -> Void in
            let txtfld : UITextField = alertController.textFields!.first!
            if txtfld.text != "" {
                self.acceptRequestWithJson(["request_id" : data.requestId, "relationship" : txtfld.text!])
            }
            
        }
        alertController.addAction(alertNoAction)
        alertController.addAction(alertYesAction)
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    //MARK:- ACCEPT REQUEST WITH JSON
    func acceptRequestWithJson(dict : NSDictionary)  {
        SVProgressHUD.showWithStatus("Processing...")
        Relationship.acceptRequest(dict as! [String : String], success: { (result) in
            SVProgressHUD.showSuccessWithStatus("Success")
            self.isSearching = false
            self.fetchRequest(false)
            }) { (error) in
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
        }
    
        
    }
}
