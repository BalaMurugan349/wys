//
//  RelativesTableViewController.swift
//  WYS
//
//  Created by Bala Murugan on 7/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RelativesTableViewController: UITableViewController {
    var arrayData : [Relationship] = [Relationship]()
    var isSearching : Bool = false
    var arraySearch : [Relationship] = [Relationship]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //fetchRelatives()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        fetchRelatives()
    }
    
    func startSearch(text : String) {
        let predicate : NSPredicate = NSPredicate(format: "name contains[C] %@", text)
        arraySearch = (arrayData as NSArray).filteredArrayUsingPredicate(predicate) as! [Relationship]
        self.tableView.reloadData()
        
    }
    
    func fetchRelatives()  {
        //SVProgressHUD.showWithStatus("Processing...")
        Relationship.myRelatives(["user_id" : Person.currentPerson().userid], success: { (result) in
            self.arrayData.removeAll()
            self.arrayData.appendContentsOf(result as! [Relationship])
            self.tableView.reloadData()
            SVProgressHUD.dismiss()
            }) { (error) in
                SVProgressHUD.dismiss()
        }
    }
    
    //MARK:- TABLEVIEW DELEGATES
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching ? arraySearch.count : arrayData.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        if(cell == nil){
            cell = RelativesTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        let data : Relationship = isSearching ? arraySearch[indexPath.row]  : arrayData[indexPath.row]
        (cell as! RelativesTableViewCell).labelName.text = data.name
        (cell as! RelativesTableViewCell).labelRelation.text = data.relationship
        (cell as! RelativesTableViewCell).imageViewRelation.image = UIImage(named: "Brother")
        (cell as! RelativesTableViewCell).imageViewProfile.image(data.profilePic)
        (cell as! RelativesTableViewCell).buttonAccept.hidden = true
        (cell as! RelativesTableViewCell).labelPoints.text = "WYS Score \n \(data.wysScore.stringByReplacingOccurrencesOfString("+", withString: ""))"

        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let chat : ChatViewController = storyboard?.instantiateViewControllerWithIdentifier("ChatVC") as! ChatViewController
       chat.userDetails =  isSearching ? arraySearch[indexPath.row] : arrayData[indexPath.row]
        AppDelegate.sharedInstance().mainNavigation.pushViewController(chat, animated: true)
        
    }
}
