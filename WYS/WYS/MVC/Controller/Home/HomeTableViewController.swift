//
//  HomeTableViewController.swift
//  WYS
//
//  Created by Bala Murugan on 7/25/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class HomeTableViewController: UIViewController {
    
    var pageControl : ADPageControl!
    
    var arrayVC : [UITableViewController]!
    @IBOutlet weak var heightConstraint : NSLayoutConstraint!
    @IBOutlet weak var searchBarRelatives : UISearchBar!
    var currentIndex : Int32 = 0
    
    var relativesVC : RelativesTableViewController!
    var requestVC : RequestsTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "WYS"
        
        relativesVC = self.storyboard?.instantiateViewControllerWithIdentifier("RelativesVC") as! RelativesTableViewController
        
        requestVC = self.storyboard?.instantiateViewControllerWithIdentifier("RequestVC") as! RequestsTableViewController
        arrayVC = [relativesVC,requestVC]
        setupPageControl()
        heightConstraint.constant = 0
        
        let btnDone : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Search"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(HomeTableViewController.onSearchButtonPressed))
        self.navigationItem.rightBarButtonItem = btnDone
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        heightConstraint.constant = 0
        searchBarRelatives.text = ""
        relativesVC.isSearching = false
        requestVC.isSearching = false
        requestVC.tableView.reloadData()
        relativesVC.tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        searchBarRelatives.resignFirstResponder()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func onSearchButtonPressed()  {
        relativesVC.isSearching = false
        relativesVC.tableView.reloadData()
        requestVC.isSearching = false
        requestVC.tableView.reloadData()
        searchBarRelatives.resignFirstResponder()
        searchBarRelatives.text = ""

        //
        if heightConstraint.constant == 0 {
            self.view.layoutIfNeeded()
            heightConstraint.constant = 44
            UIView.animateWithDuration(0.25, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        } else {
            self.view.layoutIfNeeded()
            heightConstraint.constant = 0
            UIView.animateWithDuration(0.25, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
        
    }
    func setupPageControl (){
        //page 0
        let pageModel0 = ADPageModel()
        pageModel0.strPageTitle = "RELATIONSHIP"
        pageModel0.iPageNumber = 0
        pageModel0.viewController = arrayVC[0]
        
        let pageModel1 = ADPageModel()
        pageModel1.strPageTitle = "REQUESTS"
        pageModel1.iPageNumber = 1
        pageModel1.bShouldLazyLoad = true
        
        pageControl = ADPageControl()
        pageControl.delegateADPageControl = self
        pageControl.arrPageModel = NSMutableArray(objects: pageModel0,pageModel1)
        
        /**** 3. Customize parameters (Optinal, as all have default value set) ****/
        
        //pageControl.iFirstVisiblePageNumber = 2;
        pageControl.iTitleViewHeight = 44;
        pageControl.iPageIndicatorHeight = 2;
        pageControl.fontTitleTabText =  UIFont(name: "Myriad Hebrew", size: 13.0)
        
        pageControl.bEnablePagesEndBounceEffect = false;
        pageControl.bEnableTitlesEndBounceEffect = false;
        
        pageControl.colorTabText = UIColor.whiteColor()
        pageControl.colorTitleBarBackground =  UIColor(red: 61.0/255.0, green: 136.0/255.0, blue: 193.0/255.0, alpha: 1.0)
        pageControl.colorPageIndicator = UIColor.whiteColor()
        pageControl.colorPageOverscrollBackground = UIColor.lightGrayColor()
        
        pageControl.bShowMoreTabAvailableIndicator = false;
        
        /**** 3. Add as subview ****/
        
        pageControl.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
        self.view.addSubview(pageControl.view)
        self.view.bringSubviewToFront(searchBarRelatives)
        
    }
    
    
}

extension HomeTableViewController : UISearchBarDelegate{
    //MARK:- UISearchBar Delegate
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        
        if currentIndex == 0{
            relativesVC.isSearching = true
        }else {
            requestVC.isSearching = true
        }
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.endEditing(true)
        if currentIndex == 0{
            relativesVC.isSearching = false
            relativesVC.tableView.reloadData()
        }else{
            requestVC.isSearching = false
            requestVC.tableView.reloadData()
            
        }
        
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        if currentIndex == 0{
            relativesVC.isSearching = false
            relativesVC.tableView.reloadData()
        }else{
            requestVC.isSearching = false
            requestVC.tableView.reloadData()
            
        }
        
        
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if currentIndex == 0{
            relativesVC.isSearching = true
            relativesVC.tableView.reloadData()}
        else{
            requestVC.isSearching = true
            requestVC.tableView.reloadData()
            
        }
        
        
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if currentIndex == 0{
            relativesVC.isSearching = true
            relativesVC.startSearch(searchBar.text!) }
        else{
            requestVC.isSearching = true
            requestVC.startSearch(searchBar.text!) }
        
    }
    
}

//MARK:- PAGE CONTROL DELEGATE
extension HomeTableViewController : ADPageControlDelegate{
    func adPageControlGetViewControllerForPageModel(pageModel: ADPageModel!) -> UIViewController! {
        return arrayVC[Int(pageModel.iPageNumber)]
    }
    func adPageControlCurrentVisiblePageIndex(iCurrentVisiblePage: Int32) {
        currentIndex = iCurrentVisiblePage
    }
}
