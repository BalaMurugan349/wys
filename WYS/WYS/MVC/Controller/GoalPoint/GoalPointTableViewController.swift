//
//  GoalPointTableViewController.swift
//  WYS
//
//  Created by Bala Murugan on 8/5/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class GoalPointTableViewController: UITableViewController {
    
    @IBOutlet weak var textfieldGoal : UITextField!
    @IBOutlet weak var textviewGoal : UITextView!
    @IBOutlet weak var toolBar : UIToolbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btnDone : UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(GoalPointTableViewController.DoneButtonPressed))
        self.navigationItem.rightBarButtonItem = btnDone
        self.navigationItem.title = "WYS"
        textfieldGoal.setLeftPadding(20)
//        textviewGoal.layer.cornerRadius = 3.0
//        textviewGoal.clipsToBounds = true
        textfieldGoal.inputAccessoryView = toolBar
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MAK:- DONE NAVIGATION BUTTON ACTION
    func DoneButtonPressed()  {
        if textfieldGoal.isEmpty{
            let alert = BMExtension.alert("Please enter your Goal point")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if textviewGoal.isEmpty || textviewGoal.text == "Description"{
            let alert = BMExtension.alert("Please enter your description")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            self.navigationController?.popViewControllerAnimated(true)
        }
        
    }
    
    //MARK:- DONE TOOLBAR BUTTON ACTION
    @IBAction func onDoneToolbarButtonPressed()  {
        textfieldGoal.resignFirstResponder()
    }
    
}

//MARK:- UITEXTFIELD DELEGATE
extension GoalPointTableViewController : UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

//MARK:- UITEXTVIEW DELEGATE
extension GoalPointTableViewController : UITextViewDelegate{
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "Description" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Description"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
}
