//
//  RecentActivityTableViewController.swift
//  WYS
//
//  Created by Bala Murugan on 9/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RecentActivityTableViewController: UITableViewController {
    var arrayRecentActivity : NSArray!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "RECENT ACTIVITY"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayRecentActivity.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        if(cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        let dict : NSDictionary = arrayRecentActivity[indexPath.row] as! NSDictionary
        let label1 : UILabel = cell?.viewWithTag(100) as! UILabel
        label1.text = "Gave \(dict["points"] as! String)  pt"
        
        let label2 : UILabel = cell?.viewWithTag(101) as! UILabel
        label2.text = CheckForNull(strToCheck:NSDate.changeServerTimeZoneToLocalForDate(dict["date"] as! String))

        
        return cell!
    }
    

}
