//
//  Person.swift
//  WYS
//
//  Created by Bala Murugan on 7/25/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
var _person : Person?

class Person: NSObject {
    var name : String!
    var username : String!
    var profileImage : String!
    var userid : String!
    var gender : String!
    var email : String!
    var dateOfBirth : String!

    init(details : NSDictionary) {
        super.init()
        name = CheckForNull(strToCheck: details.checkValueForKey(key: "name") as? String)
        username = CheckForNull(strToCheck: details.checkValueForKey(key: "username") as? String)
        profileImage = CheckForNull(strToCheck: details.checkValueForKey(key: "image") as? String)
        userid = CheckForNull(strToCheck: details.checkValueForKey(key: "id") as? String)
        gender = CheckForNull(strToCheck: details.checkValueForKey(key: "gender") as? String)
        email = CheckForNull(strToCheck: details.checkValueForKey(key: "email") as? String)
        dateOfBirth = CheckForNull(strToCheck: details.checkValueForKey(key: "dob") as? String)
    }

    
    //MARK:- STORE USERDETAILS
    class func initWithDictionary(dict : NSDictionary) -> AnyObject
    {
        _person = Person(details: dict)
        return _person!
    }
    
    //MARK:- CURRENT PERSON SINGLETON
    class func currentPerson() -> Person
    {
        if (_person == nil)
        {
            _person = Person(details: NSDictionary())
        }
        return _person!
    }
    
    //MARK:- LOGOUT
    class func logout(){
        _person = nil
    }
    
    class func userWithDetails (userDetails : NSDictionary) -> Person{
        
        if (_person == nil) {
            Person.saveLoggedUserdetails(userDetails)
            _person = Person.initWithDictionary(userDetails) as? Person
        }
        return _person!
    }

    //MARK:- SIGN UP
    class func signUp(details : [String : String] , profilePic : UIImage, success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        ServiceManager.fetchDataFromService("insert-user.php", parameters: details, image: profilePic, success: { (result) in
            if (result["error"] as! Bool) == false {
               let person =  Person.userWithDetails(result["details"] as! NSDictionary)
                success(result: person)
            } else {
                failure(error: NSError(errorMessage: result["failure"] as! String))
            }

            }) { (error) in
                failure(error: error)

        }
    }

    //MARK:- LOGIN
    class func login(details : [String : String] ,  success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        ServiceManager.fetchDataFromService("login.php", parameters: details, image: nil, success: { (result) in
            if (result["error"] as! Bool) == false {
                let person =  Person.userWithDetails(result["details"] as! NSDictionary)
                success(result: person)
            } else {
                failure(error: NSError(errorMessage: result["failure"] as! String))
            }
            
        }) { (error) in
            failure(error: error)
            
        }
    }

    //MARK:- FACEBOOK LOGIN
    class func facebookLogin(details : [String : String] , profilePic : UIImage, success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        ServiceManager.fetchDataFromService("fblogin.php", parameters: details, image: profilePic, success: { (result) in
            if (result["error"] as! Bool) == false {
                let person =  Person.userWithDetails(result["details"] as! NSDictionary)
                success(result: person)
            } else {
                failure(error: NSError(errorMessage: result["failure"] as! String))
            }
            
        }) { (error) in
            failure(error: error)
            
        }
    }

    //MARK:- LOGOUT
    class func logout(success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        ServiceManager.fetchDataFromService("logout.php", parameters: ["user_id" : Person.currentPerson().userid], image: nil, success: { (result) in
            if (result["error"] as! Bool) == false {
                success(result: result)
            } else {
                failure(error: NSError(errorMessage: result["failure"] as! String))
            }
            
        }) { (error) in
            failure(error: error)
            
        }
    }

    
    //MARK:- LIST USERS
    class func listUsers(details : [String : String] , success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        ServiceManager.fetchDataFromService("list.php", parameters: details, image: nil, success: { (result) in
            if (result["error"] as! Bool) == false {
                let users = Person.storeUserDetails(result["details"] as? [NSDictionary])
                if let _ = users {
                success(result: users!)
                }else{
                    failure(error: NSError(errorMessage: "No users found"))
                }
            } else {
                failure(error: NSError(errorMessage: result["failure"] as! String))
            }
            
        }) { (error) in
            failure(error: error)
            
        }
    }

    class func storeUserDetails (details : [NSDictionary]?) -> [Person]?{
        guard let arrayObjects = details where  arrayObjects.count > 0 else {
            // Value requirements not met, do something
            return nil
        }
        var personObj = [Person]()
        for dict in arrayObjects {
            let obj = Person(details: dict)
            personObj.append(obj)
        }
        return personObj

    }
    
    class func saveLoggedUserdetails(dictDetails : NSDictionary){
        let data : NSData = NSKeyedArchiver.archivedDataWithRootObject(dictDetails)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "LoggedUserDetails")
        NSUserDefaults.standardUserDefaults().synchronize()
    }


}
