//
//  BMExtension.swift
//  WYS
//
//  Created by Bala Murugan on 8/5/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit
import SDWebImage

class BMExtension: NSObject {

    //MARK:- ALERT CONTROLLER
    class func alert(message : String) -> UIAlertController {
        let alertController = UIAlertController(title: "WYS", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Destructive) { (action) -> Void in
            
        }
        alertController.addAction(alertOkAction)
        return alertController
    }

}

//MARK:- ERROR
extension NSError {
    
    convenience init(errorMessage : String) {
        self.init(domain: "Error", code: 101, userInfo: [NSLocalizedDescriptionKey : errorMessage])
    }
    
}


//MARK:- UITEXTFIELD
extension UITextField {
    var isEmpty : Bool {
        return self.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0
}
    
}

//MARK:- UITEXTVIEW
extension UITextView {
    var isEmpty : Bool {
        return self.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0
    }
}

//MARK:- UIIMAGEVIEW
extension UIImageView {
    func image(url: String) {
        self.sd_setImageWithURL(NSURL(string: url), placeholderImage: UIImage(named: "ProfilePlaceholder"), options: .RefreshCached) { (image, error, cacheType, url) in
            
        }
    }
}

//MARK:- NSDICTIONARY
extension NSDictionary {
    
    func checkValueForKey(key key: Key) -> Value? {
        // if key not found, replace the nil with
        // the first element of the values collection
        return self[key] ?? nil
        // note, this is still an optional (because the
        // dictionary could be empty)
}
}


extension String{
    var isValidEmail : Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(self)
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.max)
        let boundingBox = self.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.height
    }


}

