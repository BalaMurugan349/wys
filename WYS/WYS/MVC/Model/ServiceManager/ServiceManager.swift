//
//  ServiceManager.swift
//  REHAB
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016 REHAB. All rights reserved.
//

import UIKit
import Alamofire

public let mainUrl = "http://www.wysconnect.info/wys/api/"//"http://52.26.132.223/wys/api/"

class ServiceManager: NSObject {

    class func fetchDataFromService(serviceName: String, parameters : [String : String]? , image : UIImage?, success:(result : AnyObject) -> Void, failure :(error : NSError) -> Void) {
        
        Alamofire.upload(.POST, "\(mainUrl)\(serviceName)", multipartFormData: {
            multipartFormData in
            if let _image = image{
                if let imageData = UIImagePNGRepresentation(_image) {
                    multipartFormData.appendBodyPart(data: imageData, name: "image", fileName: makeFileName() , mimeType: "image/png")
                }
            }
            for (key, value) in parameters! {
                multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
            }
            }, encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { response in
                        //debugPrint(response)
                       let result = response.result.value
                       let error = response.result.error
                       if result != nil && error == nil {
                           success(result: result!)
                       } else if error != nil {
                           failure(error: error!)
                       } else {
                           failure(error: NSError(errorMessage: "Something went wrong"))
                       }
                    }
                case .Failure(let encodingError):
                    print(encodingError)
                    failure(error: NSError(errorMessage: "Your internet connection appears to be offline"))

                }
            }
        )

        
        
//        Alamofire.request(.GET, "\(mainUrl)\(serviceName)", parameters: parameters)
//            .responseJSON { response in
//                
//                let result = response.result.value
//                let error = response.result.error
////                let string = String(data: response.data!, encoding: NSUTF8StringEncoding)
////                print(string)
//                if result != nil && error == nil {
//                    success(result: result!)
//                } else if error != nil {
//                    failure(error: error!)
//                } else {
//                    failure(error: NSError(errorMessage: "Something went wrong"))
//                }
//        }
    }
    
   class func makeFileName() -> String{
        let dateFormatter : NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyMMddHHmmssSSS"
        let dateString = dateFormatter.stringFromDate(NSDate())
        let randomValue  = arc4random() % 1000
        let fileName : String = "B\(dateString)\(randomValue).png"
        return fileName
    }

}


