//
//  Relationship.swift
//  WYS
//
//  Created by Bala Murugan on 8/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Relationship: NSObject {
    
    var userid : String!
    var name : String!
    var username : String!
    var email : String!
    var dateOfBirth : String!
    var gender : String!
    var profilePic : String!
    var requestId : String!
    var relationship : String!
    var wysScore : String!

    init(details : NSDictionary) {
        super.init()
        userid = CheckForNull(strToCheck: details.checkValueForKey(key: "id") as? String)
        name = CheckForNull(strToCheck: details.checkValueForKey(key: "name") as? String)
        username = CheckForNull(strToCheck: details.checkValueForKey(key: "username") as? String)
        email = CheckForNull(strToCheck: details.checkValueForKey(key: "email") as? String)
        dateOfBirth = CheckForNull(strToCheck: details.checkValueForKey(key: "dob") as? String)
        gender = CheckForNull(strToCheck: details.checkValueForKey(key: "gender") as? String)
        profilePic = CheckForNull(strToCheck: details.checkValueForKey(key: "image") as? String)
        requestId = CheckForNull(strToCheck: details.checkValueForKey(key: "request_id") as? String)
        relationship = CheckForNull(strToCheck: details.checkValueForKey(key: "relationship") as? String)
        wysScore = CheckForNull(strToCheck: details.checkValueForKey(key: "wys_score") as? String)


    }

    class func getAllData(arrayObjects : [NSDictionary]?) -> [Relationship]? {
        guard let arrayObjects = arrayObjects where  arrayObjects.count > 0 else {
            // Value requirements not met, do something
            return nil
        }
        var relations = [Relationship]()
        for dict in arrayObjects {
            let relation = Relationship(details: dict as! [String: AnyObject])
            relations.append(relation)
        }
        return relations
    }

    
    //MARK:- SEND REQUEST
    class func sendRequest(details : [String : String] , success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        ServiceManager.fetchDataFromService("send-request.php", parameters: details, image: nil, success: { (result) in
            if (result["error"] as! Bool) == false {
                success(result: result)
            } else {
                failure(error: NSError(errorMessage: result["failure"] as! String))
            }
            
        }) { (error) in
            failure(error: error)
            
        }
    }
    
    //MARK:- VIEW REQUEST
    class func viewRequest(details : [String : String] , success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        ServiceManager.fetchDataFromService("view-request.php", parameters: details, image: nil, success: { (result) in
            
            if (result["error"] as! Bool) == false {
                let relations = Relationship.getAllData(result["details"] as? [NSDictionary])
                if let _ = relations {
                    success(result: relations!)
                } else {
                    failure(error: NSError(errorMessage: "No data found"))
                }
                
            } else {
                failure(error: NSError(errorMessage: result["success"] as! String))
            }
            
        }) { (error) in
            failure(error: error)
            
        }

    }

    
    //MARK:- ACCEPT REQUEST
    class func acceptRequest(details : [String : String] , success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        ServiceManager.fetchDataFromService("accept-request.php", parameters: details, image: nil, success: { (result) in
            if (result["error"] as! Bool) == false {
                success(result: result)
            } else {
                failure(error: NSError(errorMessage: result["failure"] as! String))
            }
            
        }) { (error) in
            failure(error: error)
            
        }
    }
    
    
    //MARK:- MY RELATIVES
    class func myRelatives(details : [String : String] , success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        ServiceManager.fetchDataFromService("relatives.php", parameters: details, image: nil, success: { (result) in
            
            if (result["error"] as! Bool) == false {
                let relations = Relationship.getAllData(result["details"] as? [NSDictionary])
                if let _ = relations {
                    success(result: relations!)
                } else {
                    failure(error: NSError(errorMessage: "No data found"))
                }
          } else {
                failure(error: NSError(errorMessage: "No data found"))
            }
            
        }) { (error) in
            failure(error: error)
            
        }
        
    }



}
