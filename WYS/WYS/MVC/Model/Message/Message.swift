//
//  Message.swift
//  SelfieSwap
//
//  Created by Bala on 12/03/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

class Message: NSObject {
   
    var messageId : String!
    var messageText : String!
    var messageImage : UIImage!
    var messageTime : NSString!
    var userId : String!
    var userName : String!
    var profileImageStr : String!
    var messageImageStr : String!
    var points : String = "0"

    
   class func initWithDictionary (dict : NSDictionary) -> Message{
        let messg : Message = Message()
        messg.messageId = CheckForNull(strToCheck: dict.checkValueForKey(key: "id") as? String)
        messg.messageText = CheckForNull(strToCheck: dict.checkValueForKey(key: "message") as? String)
        messg.userId = CheckForNull(strToCheck: dict.checkValueForKey(key: "from_id") as? String)
        messg.messageImageStr = ""// CheckForNull(strToCheck: dict.checkValueForKey(key: "image") as? String)
        messg.messageTime = CheckForNull(strToCheck: dict.checkValueForKey(key: "created_at") as? String)
        messg.userName = CheckForNull(strToCheck: dict.checkValueForKey(key: "name") as? String)
        messg.profileImageStr = CheckForNull(strToCheck: dict.checkValueForKey(key: "image") as? String)
        messg.points = CheckForNull(strToCheck: dict.checkValueForKey(key: "points") as? String)
  
        return messg
    }

   
    //MARK:- INSERT MESSAGE
    class func insertNewMessage(otherUserId : String , messageText : String , attachedImage : UIImage?, completion :  (Bool ,NSError?) -> Void){
        let details : [String : String] = ["from_id" : Person.currentPerson().userid, "to_id" : otherUserId, "message" : messageText, "points" : "0"]
        ServiceManager.fetchDataFromService("insert-chat.php", parameters: details, image: nil, success: { (result) in
            if (result["error"] as! Bool) == false {
                        completion(true,nil)
            } else {
                completion(false,NSError(errorMessage: "Please try again"))
            }
            
        }) { (error) in
            completion(false,NSError(errorMessage: "Please try again"))
            
        }
    }

    //MARK:- GET MESSAGE
    class func getMessage(index : String ,otherUserId : String , lastMessageId : String,completion :  (Bool ,AnyObject?, NSError?) -> Void){
        var fileName : String = ""
        var details : [String : String] = ["" : ""]
        if lastMessageId == ""
        {
            details = ["from_id" : Person.currentPerson().userid, "to_id" : otherUserId, "index" : index]
            fileName = "list-chat.php"
        }else{
            details = ["from_id" : Person.currentPerson().userid, "to_id" : otherUserId, "message_id" : lastMessageId]
            fileName = "recent-message.php"
        }
        ServiceManager.fetchDataFromService(fileName, parameters: details, image: nil, success: { (result) in
            if (result["error"] as! Bool) == false {
               completion(true,Message.storeMessages(result["details"] as! NSArray),nil)
            } else {
                
                completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No Messages", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }
            
        }) { (error) in
            completion(false,nil,error)
            
        }

}
    
    class func storeMessages(arrayPost : NSArray) -> NSArray
    {
        let array = NSMutableArray()
        (arrayPost.reverseObjectEnumerator().allObjects as NSArray).enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            let messg : Message = Message.initWithDictionary(obj as! NSDictionary)
            array.addObject(messg)
        }
        return array
    }
    
    
    //ADD POINTS
    class func addPoints(messageId : String , points : String , completion :  (Bool ,NSError?) -> Void){
        let details : [String : String] = ["message_id" : messageId, "points" : points]
        ServiceManager.fetchDataFromService("add-points.php", parameters: details, image: nil, success: { (result) in
            if (result["error"] as! Bool) == false {
                completion(true,nil)
            } else {
                completion(false,NSError(errorMessage: "Please try again"))
            }
            
        }) { (error) in
            completion(false,NSError(errorMessage: "Please try again"))
            
        }
    }


   class func makeFileName() -> NSString{
        let dateFormatter : NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyMMddHHmmssSSS"
        let dateString = dateFormatter.stringFromDate(NSDate())
        let randomValue  = arc4random() % 1000
        let fileName : NSString = "B\(dateString)\(randomValue).jpg"
        return fileName
    }
}
