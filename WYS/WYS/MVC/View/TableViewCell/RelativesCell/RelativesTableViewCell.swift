//
//  RelativesTableViewCell.swift
//  WYS
//
//  Created by Bala Murugan on 7/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RelativesTableViewCell: UITableViewCell {

    var viewBackground : UIView!
    var labelName : UILabel!
    var imageViewProfile : UIImageView!
    var viewSeparator : UIView!
    var buttonAccept : UIButton!
    var labelRelation : UILabel!
    var imageViewRelation : UIImageView!
    var labelPoints : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        viewBackground  = UIView(frame: CGRectMake(0, 0, screenSize.width, 90))
        viewBackground.backgroundColor = UIColor.clearColor()
        self.contentView.addSubview(viewBackground)
        
        imageViewProfile = UIImageView(frame: CGRectMake(10, 20, 50, 50))
        imageViewProfile.layer.cornerRadius = 25.0
        imageViewProfile.clipsToBounds = true
        imageViewProfile.image = UIImage(named: "ProfilePlaceholder")
        imageViewProfile.contentMode = .ScaleAspectFill
        viewBackground.addSubview(imageViewProfile)
        
        labelName = UILabel(frame: CGRectMake(70, 20, screenSize.width - 185, 50))
        labelName.textColor = UIColor.blackColor()
        labelName.font = UIFont.systemFontOfSize(15.0)
        labelName.adjustsFontSizeToFitWidth = true
        viewBackground.addSubview(labelName)
        
        buttonAccept = UIButton(type: UIButtonType.Custom)
        buttonAccept.setTitle("ACCEPT", forState: UIControlState.Normal)
        buttonAccept.titleLabel?.font = UIFont.systemFontOfSize(12.0)
        buttonAccept.setBackgroundImage(UIImage(named: "AcceptButton"), forState: UIControlState.Normal)
        buttonAccept.frame = CGRectMake(screenSize.width - 95, 35, 80, 25)
        viewBackground.addSubview(buttonAccept)
        
        labelRelation = UILabel(frame: CGRectMake(screenSize.width - 60, 55, 60, 20))
        labelRelation.textColor = UIColor(red: 62.0/255.0, green: 153.0/255.0, blue: 223.0/255.0, alpha: 1.0)
        labelRelation.font = UIFont.boldSystemFontOfSize(10.0)
        labelRelation.textAlignment = .Center
        labelRelation.adjustsFontSizeToFitWidth = true
        viewBackground.addSubview(labelRelation)

        imageViewRelation = UIImageView(frame: CGRectMake(screenSize.width - 43, 30, 25, 25))
        imageViewRelation.contentMode = .ScaleAspectFit
        viewBackground.addSubview(imageViewRelation)
        
        //LABEL POINTS
        labelPoints = UILabel(frame: CGRectMake(screenSize.width - 115, 0, 55, 90))
        labelPoints.text = "WYS Score \n 0"
        labelPoints.textAlignment = NSTextAlignment.Center
        labelPoints.textColor = UIColor.blackColor()
        labelPoints.font = UIFont.systemFontOfSize(10.0)
        labelPoints.lineBreakMode = NSLineBreakMode.ByWordWrapping
        labelPoints.numberOfLines = 0
        labelPoints.adjustsFontSizeToFitWidth = true
        viewBackground.addSubview(labelPoints)


    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
