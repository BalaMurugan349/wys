//
//  PreferenceTableViewCell.swift
//  WYS
//
//  Created by Bala Murugan on 8/3/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PreferenceTableViewCell: UITableViewCell {
    
    var imageViewBackground : UIImageView!
    var labelPreference : UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        let viewBackground  = UIView(frame: CGRectMake(0, 0, screenSize.width, 60))
        viewBackground.backgroundColor = UIColor.clearColor()
        self.contentView.addSubview(viewBackground)
        
        imageViewBackground = UIImageView(frame: CGRectMake(40, 5, screenSize.width - 80, 50))
        imageViewBackground.clipsToBounds = true
        imageViewBackground.image = UIImage(named: "TextfieldBG")
        imageViewBackground.contentMode = .ScaleAspectFit
        viewBackground.addSubview(imageViewBackground)
        
        labelPreference = UILabel(frame: CGRectMake(50, 5, screenSize.width - 100, 50))
        labelPreference.textColor = UIColor.whiteColor()
        labelPreference.font = UIFont.systemFontOfSize(13.0)
        labelPreference.numberOfLines = 0
        labelPreference.lineBreakMode = .ByWordWrapping
        viewBackground.addSubview(labelPreference)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
