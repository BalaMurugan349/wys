//
//  ChatTableViewCell.swift
//  SelfieSwap
//
//  Created by Bala on 13/03/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

protocol chatCellDelegate{
 func didTapImage (messageAttachedImage : UIImage)

}
class ChatTableViewCell: UITableViewCell {

    var messageView : UIView!
    var messageLabel : UILabel!
    var messageStatusLabel : UILabel!
    var personImageView : UIImageView!
    var messageAttachedImage : UIImageView!
    var balloonView : UIImageView!
    var isSent : Bool!
    var message : Message!
    var messageCopy : Message!
    var cellDelegate : chatCellDelegate!
    let textMarginHorizontal : CGFloat = 17.5
    let textMarginVertical : CGFloat = 12.5
    let  messageTextSize : CGFloat = 17.0

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- MAXIMUM MESSAGE WIDTH
   class func maxTextWidth () -> CGFloat{
        return UIDevice.currentDevice().userInterfaceIdiom == .Phone ? 195.0 : 600.0
    }
    
    //MARK:- CALCULATE HEIGHT FOR LABEL
    class func heightForLabel ( text:String, font:UIFont, width:CGFloat) -> CGSize{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.size
    }
    
    //MARK:- BALLOON IMAGE
    class func balloonImage (sent : Bool , isSelected : Bool) -> UIImage{
        let imageBalloon : UIImage = sent == false ?  UIImage(named: "senderBubble")!.stretchableImageWithLeftCapWidth(24, topCapHeight: 15) : UIImage(named: "receiverBubble")!.stretchableImageWithLeftCapWidth(24, topCapHeight: 15)
        return imageBalloon
    }
    
    //MARK:- PREPARE FOR REUSE
    override func prepareForReuse() {
        messageAttachedImage.image = nil
    }
    
    //MARK:- INIT FUNCTION
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        /*Initializing view-lements*/
        messageView = UIView(frame: CGRectZero)
        balloonView = UIImageView(frame: CGRectZero)
        messageView.autoresizingMask = UIViewAutoresizing.FlexibleHeight
        
        /*Set Image*/
         personImageView = self.setImageView()
         messageAttachedImage = self.setImageView()
        messageAttachedImage.userInteractionEnabled = true
        messageAttachedImage.hidden = true
        
        /*Message & Time-Label*/
         messageLabel = self.setLabelWithFontSize(UIFont(name: "HelveticaNeue-Light", size: messageTextSize)!)
         messageLabel.textAlignment = NSTextAlignment.Justified;
         messageStatusLabel = self.setLabelWithFontSize(UIFont.boldSystemFontOfSize(12.0))
        messageStatusLabel.textColor = UIColor.lightGrayColor()

        messageView.addSubview(balloonView)
        messageView.addSubview(messageLabel)
        self.contentView.addSubview(messageView)
        self.contentView.addSubview(personImageView)
        self.contentView.addSubview(messageAttachedImage)
        self.contentView.addSubview(messageStatusLabel)
        /*Add Gestures*/
       self.setGestures()

    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK:- IMAGEVIEW
    func setImageView () -> UIImageView{
        let imageView : UIImageView = UIImageView(image: nil)
        imageView.contentMode = UIViewContentMode.ScaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 25.0
        imageView.layer.borderColor = UIColor(red: 207.0/255.0, green: 234.0/255.0, blue: 251.0/255.0, alpha: 1.0).CGColor
        imageView.layer.masksToBounds = true
        return imageView
    }
    
    //MARK:- LABELS
    func setLabelWithFontSize (font : UIFont) -> UILabel{
        let label : UILabel = UILabel(frame: CGRectZero)
        label.font = font
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.backgroundColor = UIColor.clearColor()
        return label
    }
    
    //MARK:- GESTURES
    func setGestures(){
        let tapgesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ChatTableViewCell.setUpTapGestureToShowPicture(_:)))
        tapgesture.numberOfTapsRequired = 1
        tapgesture.numberOfTouchesRequired = 1
        messageAttachedImage.addGestureRecognizer(tapgesture)
        tapgesture.delegate = self
    }
    
    //MARK:- SETTER FUNCTION
    func setNewMessage (messg : Message){
        messageCopy = messg
        let pts = messg.points == "0" ? "" : messg.points
        messageLabel.text = messg.messageText.stringByRemovingPercentEncoding!  + "  " + pts
        personImageView.image(messg.profileImageStr)
//        personImageView.sd_setImageWithURL(NSURL(string: Person.getImagePathForUserProfileImage(messg.userId as String, imageName: messg.profileImageStr as String)), placeholderImage: UIImage(named: "ProfilePlaceholder"))
        messageView.hidden = messg.messageText.characters.count == 0
        messageAttachedImage.hidden = messg.messageImageStr == nil && messg.messageImage == nil
        if (messg.messageImage == nil)
        {
//            if (messg.messageImageStr.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) > 0){
//            messageAttachedImage.sd_setImageWithURL(NSURL(string: "\(baseUrl)/uploaded_images/photos/\(messg.userId)/chat/\(messg.messageImageStr)"), placeholderImage: UIImage(named: "pin"))
//        }
        }else
        {
            messageAttachedImage.image = messg.messageImage
        }

        messageStatusLabel.text = String(format: "Delivered %@",CheckForNull(strToCheck:NSDate.changeServerTimeZoneToLocalForDate(messg.messageTime as String)))
    
        let statusSize : CGSize =  ChatTableViewCell.heightForLabel(messageStatusLabel.text == nil ? "" : messageStatusLabel.text!, font: messageStatusLabel.font, width: ChatTableViewCell.maxTextWidth() + (2 * textMarginHorizontal))
        let textHeight : CGFloat =  ChatTableViewCell.heightForLabel(messageLabel.text == nil ? "" : messageLabel.text!, font: messageLabel.font, width: ChatTableViewCell.maxTextWidth()).height
        
        let textWidth : CGFloat =  ChatTableViewCell.heightForLabel(messageLabel.text == nil ? "" : messageLabel.text!, font: messageLabel.font, width: ChatTableViewCell.maxTextWidth()).width
        
        personImageView.frame = CGRectMake(isSent == true ? (CGRectGetMinX(UIScreen.mainScreen().bounds) + 5.0) : (CGRectGetMaxX(UIScreen.mainScreen().bounds) - 55.0), CGRectGetMinY(self.bounds) + 5.0, 50.0, 50.0)

        messageView.frame = CGRectMake(isSent == true ? (CGRectGetMaxX(personImageView.frame) + 5.0) : (CGRectGetMinX(personImageView.frame) - (5.0 + (2 * textMarginHorizontal) + textWidth)), CGRectGetMinY(personImageView.frame), (2 * textMarginHorizontal) + textWidth, (2 * textMarginVertical) + textHeight)
        
        balloonView.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(messageView.frame), CGRectGetHeight(messageView.frame));
        
        messageLabel.frame = CGRectMake(textMarginHorizontal, textMarginVertical,ChatTableViewCell.maxTextWidth() , textHeight);
        messageStatusLabel.frame = CGRectMake( isSent == true ? CGRectGetMinX(messageView.frame) : CGRectGetMaxX(messageView.frame) - statusSize.width, CGRectGetMaxY(messageView.frame) + 5.0, statusSize.width , statusSize.height);

        messageAttachedImage.frame = CGRectMake(isSent == true ? CGRectGetMinX(messageStatusLabel.frame) : (CGRectGetMaxX(messageView.frame) - 110.0), CGRectGetMaxY(messageStatusLabel.frame) + 5.0, 110.0, 110.0);
        balloonView.image = ChatTableViewCell.balloonImage(isSent, isSelected: self.selected)
        
    }
    
    //MARK:- TAP GESTURE ACTION
    func setUpTapGestureToShowPicture (gestureRecogniser : UITapGestureRecognizer){
        let imageView : UIImageView = gestureRecogniser.view as! UIImageView
        cellDelegate.didTapImage(imageView.image!)
    }
}
