//
//  GoalAlertView.swift
//  WYS
//
//  Created by Bala Murugan on 8/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class GoalAlertView: UIView ,UITextFieldDelegate{

    //    @IBOutlet weak var textviewEffort : UITextView!
    @IBOutlet weak var textviewAcknowledgement : UITextView!
    @IBOutlet weak var textfieldGoal : UITextField!
    
    var completion:((String,String,Bool)->Void)?
    var errorAlert : ((String) -> Void)?
    
    static var sharedInstance : GoalAlertView {
        let instance :  GoalAlertView = NSBundle.mainBundle().loadNibNamed("GoalAlertView", owner: nil, options: nil)!.first as! GoalAlertView
        return instance
        
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "Please Enter Your Description" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Please Enter Your Description"
            textView.textColor = UIColor.lightGrayColor()
        }
    }

    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func showPopUp(inview : UIView ,completion : (goal : String , description : String , isOkSelected : Bool) -> Void , showAlert : (alertMessage : String)-> Void)  {
        self.completion = completion
        self.errorAlert = showAlert
        //        textviewEffort.text = ""
        textviewAcknowledgement.text = ""
        textfieldGoal.text = ""
        
        inview.addSubview(self)
        inview.bringSubviewToFront(self)
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    
    @IBAction func cancelButtonPressed (sender : UIButton){
//            self.completion!("" , "", true)
//            self.removeFromSuperview()
        KGModal.sharedInstance().hideAnimated(true)
    }
    
    @IBAction func doneButtonPressed (sender : UIButton){
//        self.completion!( "" , "" , false)
//        self.removeFromSuperview()
        KGModal.sharedInstance().hideAnimated(true)

    }

}
